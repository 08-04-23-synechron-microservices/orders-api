package com.classpathio.orders.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import com.classpathio.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class DBHealthIndicator implements HealthIndicator {

	private final OrderRepository orderRepository;

	@Override
	public Health health() {
		try {
			long count = this.orderRepository.count();
			return Health.up().withDetail("DB-service", "DB-service is up").build();
		} catch (Exception exception) {
			return Health.down().withDetail("DB-service", "DB-service is down").build();
		}

	}
}

@Component
class KafkaHealthIndicator implements HealthIndicator {

	@Override
	public Health health() {
		return Health.up().withDetail("Kafka-service", "Kafka-service is up").build();
	}
}
