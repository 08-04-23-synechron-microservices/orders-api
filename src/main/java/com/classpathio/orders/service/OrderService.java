package com.classpathio.orders.service;

import java.util.Map;
import java.util.Set;

import com.classpathio.orders.model.Order;

public interface OrderService {
	
	Order saveOrder(Order order);
	
	Map<String, Object> fetchAllOrders(int page, int size);
	
	Order fetchOrderById(long id);
	
	void deleteOrderById(long id);
	
	Order updateOrder(long orderId, Order updatedOrder);
}
