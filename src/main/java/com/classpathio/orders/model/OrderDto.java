package com.classpathio.orders.model;

import java.time.LocalDate;

public interface OrderDto {

	String getCustomer();

	double getTotalOrderPrice();

	LocalDate getOrderDate();

}
