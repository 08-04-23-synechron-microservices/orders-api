package com.classpathio.orders.util;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;

import com.classpathio.orders.model.LineItem;
import com.classpathio.orders.model.Order;
import com.classpathio.orders.repository.OrderRepository;
import com.github.javafaker.Faker;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
@Profile("dev")
class AppData {

	private final OrderRepository orderRepository;

	private final Faker faker = new Faker();

	@Value("${app.orderCount}")
	private int orderCount;

	@EventListener(ApplicationReadyEvent.class)
	public void handleApplicationReady(ApplicationReadyEvent event) {
		System.out.println("----------------- Application is ready -------------------");

		IntStream.range(0, orderCount).forEach(index -> {

			Order order = Order.builder().orderDate(
					faker.date().past(10, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
					.build();

			IntStream.range(0, faker.number().numberBetween(2, 5)).forEach(value -> {
				LineItem lineItem = LineItem.builder().name(faker.commerce().productName())
						.qty(faker.number().numberBetween(2, 5)).price(faker.number().randomDouble(2, 400, 800))
						.build();
				double totalPrice = order.getTotalOrderPrice() + lineItem.getPrice() * lineItem.getQty();
				order.setTotalOrderPrice(totalPrice);
				order.addLineItem(lineItem);
			});

		});
		System.out.println("----------------- Application is ready -------------------");
	}
}