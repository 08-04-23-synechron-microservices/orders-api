package com.classpathio.orders.util;

import static java.util.Set.of;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfiguration  implements CommandLineRunner {
	
	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public void run(String... args) throws Exception {
		String[] beanNames = this.applicationContext.getBeanDefinitionNames();
		
		//imperative style of coding
		/*
		 * for(String bean: beanNames) { System.out.println("Bean :: "+ bean); }
		 */
		System.out.println("*******************");
		//declarative style of coding
		of(beanNames)
		.stream()
		.filter(bean -> bean.startsWith("user"))
		.forEach(System.out::println);
		System.out.println("*******************");
		
	}

}
